package com.spring_leaning;

import java.util.List;

public class MusicPlayer {
    private List<Music> musicList;

    private String name;
    private int volume;

    public MusicPlayer(){}

    public MusicPlayer(List<Music> musicList) {
        this.musicList = musicList;
    }

    public void setMusicList(List<Music> musicList) {
        this.musicList = musicList;
    }

    public void playMusic() {
        musicList.forEach(r -> System.out.println(r.getSong()));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public void playerInitialization() {
        // Here we can create connection with database and so on
        System.out.println("Player initialization!");
    }

    public void playerDestruction() {
        // Here we can close connection with database if the scope for MusicPlayer bean is not "prototype", because in this case Spring won't call destruction method
        System.out.println("Player destruction!");
    }
}
