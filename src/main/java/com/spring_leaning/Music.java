package com.spring_leaning;

public abstract class Music {
    public abstract String getSong();

    // Method modifier can be anything and cannot receives arguments
    // We also can return anything but it doesn't make sense because we couldn't handle returned value
    public void doMyInit() {
        System.out.println(this.getClass().getSimpleName() + " initialization!");
    }

    // If the defined scope is prototype than Spring won't call destroy method
    public void doMyDestroy() {
        System.out.println(this.getClass().getSimpleName() + " destruction!");
    }

}
