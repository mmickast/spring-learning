package com.spring_leaning;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.comparator.Comparators;

public class TestSpring {
    public static void main(String... args) {
        // It's necessary to mark resources directory as a resource
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        var musicPlayer = context.getBean("musicPlayer", MusicPlayer.class); // Here we should pass bean id and class of the bean

        System.out.println("");
        musicPlayer.playMusic();

        System.out.println("Player info: name - " + musicPlayer.getName() + ", volume - " + musicPlayer.getVolume() + "\n");

        var newMusicPlayer = context.getBean("musicPlayer", MusicPlayer.class);
        System.out.println("musicPlayer is" + ((musicPlayer.equals(newMusicPlayer)) ? "" : " not") + " equals to newMusicPlayer: " + "(" + musicPlayer.hashCode() + ((musicPlayer.equals(newMusicPlayer)) ? "=" : "!") + "=" + newMusicPlayer.hashCode() + ")");

        context.close(); // We should close context
    }
}
