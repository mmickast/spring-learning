package com.spring_leaning;

public class ClassicalMusic extends Music {
    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }

    public static ClassicalMusic getClassicalMusic() {
        return new ClassicalMusic();
    }
}
